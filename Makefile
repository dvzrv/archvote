SEATS?=1

tally:
	@echo
	@python3 tally.py --seats $(SEATS)

votes/%.asc: FORCE
	@python3 verify.py $@ conf/candidates conf/all-users.csv

verify: $(wildcard votes/*.asc)

FORCE:

.PHONY: FORCE tally verify
