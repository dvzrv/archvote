#!/usr/bin/env python3

import collections
import csv
import gnupg
import os
import sys


# parse command line arguments
if len(sys.argv) != 4:
    print('usage: verify.py <votesfile> <candidatesfile> <userdb>', file=sys.stderr)
    sys.exit(1)
fn = sys.argv[1]
candidatesfn = sys.argv[2]
usersfn = sys.argv[3]

# parse user database
users = {}
with open(usersfn, newline='') as csvfile:
    usersreader = csv.reader(csvfile)
    for row in usersreader:
        if len(row) != 4:
            print('{}: invalid format'.format(usersfn), file=sys.stderr)
            sys.exit(1)
        users[row[0]] = row[1:]

# parse user database
with open(candidatesfn) as f:
    candidates = f.read().splitlines()
candidates.sort()
for candidate in candidates:
    if candidate not in users:
        print('{}: invalid user name: {}'.format(candidatesfn, candidate), file=sys.stderr)
        sys.exit(1)

# parse votes file
errors = []

with open(fn, 'r') as f:
    # verify signature and obtain public key fingerprint
    gpg = gnupg.GPG()
    verified = gpg.verify(f.read())
    if not verified:
        errors.append('invalid signature ({})'.format(verified.status))
    fingerprint = verified.pubkey_fingerprint

    # rewind file and extract message body
    f.seek(0)
    data = gpg.decrypt(f.read())
    lines = str(data).splitlines()

    if len(lines) >= 4:
        # verify user name
        username = lines[0]
        if username not in users:
            errors.append('invalid user name: {}'.format(username))

        # verify email address
        email = lines[1]
        if username in users and users[username][1] != email:
            errors.append('email address mismatch: {}'.format(email))

        # match fingerprint of the signature against the user DB
        if username in users and users[username][2] != fingerprint:
            errors.append('invalid fingerprint: {}'.format(fingerprint))

        if lines[2] != '':
            errors.append('missing empty line')

        # validate the vote part
        vote = lines[3:]
        if sorted(vote) != candidates:
            errors.append('invalid vote: {}'.format(vote))
    else:
        errors.append('invalid format')

# print status message and exit
print('verifying {}...'.format(fn), end=' ')
if errors:
    for errmsg in errors:
        print('{}: {}'.format(fn, errmsg), file=sys.stderr)
    print('[\033[91mnot ok\033[0m]')
    sys.exit(1)
else:
    print('[\033[92m\033[1mok\033[0m]')
    sys.exit(0)
