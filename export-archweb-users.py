#!/usr/bin/python

import csv

# get the groups
developers = Group.objects.get(name="Developers")
tus = Group.objects.get(name="Trusted Users")
staff = Group.objects.get(name="Support Staff")

# create a set of the users
all = set(User.objects.filter(groups__in=[developers, tus, staff]))

csv_file = open('all-users.csv', 'w')
writer = csv.writer(csv_file)

for user in all:
    writer.writerow([user.username, user.get_full_name(), user.email, user.userprofile.pgp_key])

csv_file.close()
